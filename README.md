# Install

These tools require Ubuntu Bionic (18.04)

```bash
sudo apt update && sudo apt upgrade -qy
sudo dpkg -i *.deb  # Errors about unmet dependencies are expected
sudo apt install -fqy
```

The protobuf and grpc deb files are needed as Ubuntu's versions are too old.

# Uninstall

```bash
sudo apt remove cqp-qkdsim --auto-remove
```

# QKDSim

This simulates a Site Agent with dummy QKD devices. Two instances of the program need to be run to generate key, 
perfect key is produced and made available via the IKey interface. Use the ``-r`` switch to connect the second instance
to the first.
The state of the connection can be interrogated with the ISiteDetails interface which will send a message when any state change takes place.
A key can only be requested once for each side, once as a new key (without specifying a key id) and once on the opposite side by providing the key id.

## Usage

Run an instance on a specific port

```bash
QKDSim -p 8001
```

Run a second instance and connect to the first

```bash
QKDSim -p 8002 -r localhost:8001
```

Specify a configuration file

```bash
QKDSim -c config.json
```

Sample config file

```json
{
 "name": "SiteA",
 "id": "1360cb73-7384-4a0a-a7ab-1018e1310a88",
 "deviceUrls": [
  "dummyqkd:///?side=alice",
  "dummyqkd:///?side=bob"
 ],
 "listenPort": 8001,
 "credentials": {}
}
```

# StatsGenerator

This provides dummy data via the IReporting interface to describe the current state of the system. It it intended to be
used to gain lowlevel granular details about the system operation, it is not intended to be used by key consumers and 
will be issolated from the external key based interfaces such that only clients with access to the control plain can access it.

## Usage

Run an instance on a specific port

```bash
StatsGenerator -p 8003
```

Print statistic values to the console and quit after 100 reports

```bash
StatsGenerator -l -c 100
```

# Interfaces

The interfaces are supplied as [gRPC](https://grpc.io/) proto files.
They define the messages and functions which can be called on a service.
To be used they need to be compiled to your target platform/language.
See the [Getting started documentation](https://grpc.io/docs/)
For Ubuntu, installing the `protobuf` and `grpc` deb files will give you the `protoc` program you need to compile the interfaces.

## C++

```bash
mkdir build
# make the protobuf files
protoc -I Interfaces --cpp_out=build Interfaces/*.proto
protoc -I Interfaces --grpc_out=build --plugin=protoc-gen-grpc=`which grpc_cpp_plugin` Interfaces/*.proto
```

### Get a Key

```c++
    // create a connection to the site
    std::shared_ptr<grpc::Channel> channel = grpc::CreateChannel("localhost:1234", grpc::InsecureChannelCredentials());
    // create a stub to issue calls
    std::shared_ptr<IKey::Stub> stub = IKey::NewStub(channel);

    ClientContext ctx;
    cqp::remote::KeyRequest request;
    cqp::remote::SharedKey response;
    // define the other end of the site-to-site key
    request.set_siteto("some.where:1234");

    Status getResult = stub->GetSharedKey(&ctx, request, &response);
    if(getResult.ok())
    {
        std::cout << "KeyID=" << response.keyid() << std::endl;
        // key value is in response.keyvalue();
    }
```

### Get status updates

```c++
    // create a connection to the site
    std::shared_ptr<grpc::Channel> siteChannel = grpc::CreateChannel("localhost:1234", grpc::InsecureChannelCredentials());
    // create a stub to issue calls
    std::unique_ptr<remote::ISiteDetails::Stub> detailsStub = remote::ISiteDetails::NewStub(siteChannel);
    grpc::ClientContext ctx;
    google::protobuf::Empty request;
    // open a stream for messages by calling the RPC
    auto reader = detailsStub->GetLinkStatus(&ctx, request);
    // the incomming messages will be stored here
    remote::LinkStatus message;
    // read messages
    while(reader && reader->Read(&message))
    {
        std::string stateString;
        switch (message.state())
        {
        case remote::LinkStatus_State_Inactive:
            stateString = "Inactive";
            break;
        case remote::LinkStatus_State_Connecting:
            stateString = "Connecting";
            break;
        case remote::LinkStatus_State_ConnectionEstablished:
            stateString = "ConnectionEstablished";
            break;
        case remote::LinkStatus_State_LinkStatus_State_INT_MAX_SENTINEL_DO_NOT_USE_:
        case remote::LinkStatus_State_LinkStatus_State_INT_MIN_SENTINEL_DO_NOT_USE_:
            break;
        }
        // report the state change on the console
        LOGINFO("State changed to " + stateString + " for " + message.siteto() + ", code=" + std::to_string(message.errorcode()));
    }
```

## Python

```bash
python -m grpc_tools.protoc -IInterfaces --python_out=build --grpc_python_out=build Interfaces/*.proto
```
