# Example of using the statistics interface with javascript

This is very rough js which just gives an example of using the gRPC interfaces
This uses some dependencies from npm such as:
- bonjour
- google-protobuf
- grpc
- grpc-tools
- protobufjs
- ws

The interface files were generated with `protoc -js_out ...`.

```js

const WebSocket = require('ws');

class ClientHandler {
    constructor(clientSocket) {
        this.clientSocket = clientSocket;
        this.clientSocket.on('message', this.onSocketMessage);
    }

    onSocketMessage(data) {
        console.log(data);
    }

    sendReportToClient(report) {
    	console.log(report)
    	try{
	        this.clientSocket.send(JSON.stringify(report));
	    } catch(err)
	    {
	    	console.log(err)
	    }
    }
}

class ReportingClient {
	constructor(socketPort = 9001)
	{
        console.log("ReportingClient starting");
        // this is so that class functions work, /yay javascript/ :/
        let that = this;

        this.webClients = [];
        this.grpcClients = [];
        this.currentData = {};
        this.socketPort = parseInt(socketPort);
        this.socketServer = new WebSocket.Server({
            port: this.socketPort,
        });
        console.log("Creating websocket")
        this.socketServer.on('connection', function(clientws) {
         that.onSocketConnection(clientws);
        });
        console.log('Websocket on: ' + this.socketServer.options.port);

        console.log("Connecting to bonjour");
        // see https://www.npmjs.com/package/bonjour
        let bonjour = require('bonjour')()
        // possible options for find: 
        // type (string), 
        // subtypes (array of strings), 
        // protocol (string) - defaults to tcp, 
        // txt (object) - passed into dns-txt module contructor. 
        // 		Set to { binary: true } if you want to keep the TXT records in binary
    	this.bonjourBrowser = bonjour.find({ type: 'grpc'}, function(service) {
    		console.log('Found service: ' + service.name + ' on ' + service.host + ":" + service.port);
    		that.initGrpcConnection(service.host + ':' + service.port, service.name);
    	})

        this.publisher = setInterval(function() {
            console.log(that.currentData)
            let keyLevels = [];
            // convert the data into something usful
            for(let site in that.currentData)
            {
                for(let reportKey in that.currentData[site])
                {
                    if(that.currentData[site][reportKey].pathlist === 'Key Generation,Unused Keys Available')
                    {
                        let report = that.currentData[site][reportKey];
                        let entry = {};
                        entry.left = report.parameters["from"];
                        entry.right = report.parameters["to"];
                        entry.level = report.latest;
                        entry.active = true;

                        if(report.latest == 0)
                        {
                            console.log("+++" + report)
                        }
                        keyLevels.push(entry);
                    }

                }
            }

            let report = {};
            report.keyLevels = keyLevels;
            console.log(report)


            for (let index = 0; index < that.webClients.length; index++) {
                console.log('Sending report to client ' + index);

                that.webClients[index].sendReportToClient(report);
            }
        }, 2000);
	}


    initGrpcConnection(grpcServerAddr, name) {
        // this is so that class functions work, /yay javascript/ :/
        let that = this;
        // GRPC

		var messages = require('./grpc/IReporting_pb');
		var services = require('./grpc/IReporting_grpc_pb');

        var grpc = require('grpc');

        let client = new services.IReportingClient(
            grpcServerAddr,
            grpc.credentials.createInsecure());
        this.grpcClients.push(client);

        // see https://github.com/grpc/grpc/blob/v1.6.x/examples/node/static_codegen/route_guide/route_guide_client.js
        let request = new messages.ReportingFilter();
        let filter = new messages.ReportingFilter.StatFilter();
        filter.setFullnameList(["Key Generation", "Unused Keys Available"]);
        request.addFilters(filter);
        request.setListisexclude(false);

        console.log('Calling getStatistics');
        let readStream = client.getStatistics(request);
        readStream.on('data', function(report){
        	//console.log('Got a report from the server');
		 	that.onGetStatisticsData(name, report);
        });
        readStream.on('error', function(err){
        	console.log(err);
        })
        console.log('waiting for data...');
	
    }

    onSocketConnection(clientSocket) {
        let client = new ClientHandler(clientSocket);
        this.webClients.push(client);
        //client.sendReportToClient(dummyData);
    }

    onGetStatisticsData(name, /*SiteAgentReport*/ report) {
        //console.log('Found report: "' + report);
        let simplified = {};
        simplified.siteName = name;
        simplified.units = report.getUnit();
        simplified.update = report.getUpdated().toDate();
        let val;

        simplified.path = [];
        for(let index = 0; index < report.getPathList().length; index++)
        {
            simplified.path.push(report.getPathList()[index]);
        }

        // access to the map object is guess work through trial and error
        simplified.parameters = {};
        let paramEntries = report.getParametersMap().toArray();
        
        for(let index = 0; index < paramEntries.length; index++)
        {
            simplified.parameters[paramEntries[index][0]] = paramEntries[index][1];
        }

        if(report.hasAsdouble())
        {
            val = report.getAsdouble();
        } else if(report.hasAslong())
        {
            val = report.getAslong();
        } else if(report.hasAsunsigned())
        {
            val = report.getAsunsigned();
        } else {
            console.log("Unknown unit type in report");
        }

        if(typeof(val) !== 'undefined')
        {
            simplified.id = report.getId();
            simplified.latest = val.getLatest();
            simplified.average = val.getAverage();
            simplified.total = val.getTotal();
            simplified.min = val.getMin();
            simplified.max = val.getMax();
            simplified.pathlist = report.getPathList().toString();
            if(typeof(this.currentData[simplified.siteName]) === 'undefined')
            {
                console.log("Creating new site: " + simplified.siteName)
                this.currentData[simplified.siteName] = {};
            }
            this.currentData[simplified.siteName][report.getId()] = simplified;
        }

    } // onGetStatisticsData

}

module.exports.ReportingClient = ReportingClient;
```