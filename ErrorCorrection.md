# Error Correction Algorithm

This should be Written in C++11/14.  OpenCL could provide improved performance.

## Inputs

Data is passed to the error correction algorithm from the previous stage as a vector of bytes and spare bits.
Parameters should be passed to the function to control the algorthm.
The algoriuthm will need to communicate with the other side, it is assumed that the CorrectDataBlock function will 
only be called on one side. This function will generate the syndrome then call RemoteCorrection (in the real system this will be a remote gRPC call) a number of times until the data is corrected.
The RemoteCorrection function will access the local collected data and perform the correction and store the corrected code in another variable.
It will be nessacary to generate many different codes to find the best one. Different codes may need to be used based on the estimated error rate.

## Sugested signiture

```c++
/// Definiton of a generic/opaque block of data.
using DataBlock = std::vector<uint8_t>;

struct LDPCParameters
{
    /// The error rate from previous runs
    /// this could be used to affect the choice of code used on this run.
    double previousError;
    // TBD
};

/**
* Perform the LDPC algorithm on the data. 
* @detail After this call, the output data on this side will be bit-for-bit identical to the remote copy of the data
* @param[in,out] inputData The uncorrected data which is aligned with data at the other side. The length of this data block will, If this function succeeds, this will contain the corrected data
* match the corresponding block of data on the remote site.
* @param[in] params Settings which control the behavour of the process
**/
void CorrectDataBlock(DataBlock& inputData, const LDPCParameters& params)
{
    do
    {
        // select code
        // generate syndrome
        CorrectionResult result = RemoteCorrection(input);
        // correct data
    } while(!correctionComplete)
}

struct ParityMessage {
    /// The number of bytes to correct from the collected data
    uint64_t numberOfInputBytes;
    // TBD
}

struct CorrectionResult
{
    // TBD
}
    
DataBlock incommingData;
DataBlock correctedData;

/**
* Perform the remote side of the error corection
* @param input The encoded bytes needed to perform the correction on the local data
* @return Any result information
*/
CorrectionResult RemoteCorrection(const ParityMessage& input)
{
    // correct local copy of data
    correctedData.assign(incommingData.begin, incommingData.begin + input.numberOfInputBytes);
    // ... 
    
}
```
